<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employee_attendances', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->foreignId('employee_id');
            $table->date('attendance_date');
            $table->boolean('is_present')->default(0);
            $table->boolean('is_absent')->default(0);
            $table->boolean('in_leave')->default(0);
            $table->timestamps();

            $table->foreign('employee_id')->on('employees')->references('id')->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employee_attendances');
    }
};
