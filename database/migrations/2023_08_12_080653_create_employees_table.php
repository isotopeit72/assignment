<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->string('name', 128);
            $table->string('gender', 32);
            $table->date('date_of_birth');
            $table->date('date_of_joining');
            $table->string('mobile_no', 32);
            $table->string('father_name', 128);
            $table->string('department', 64);
            $table->unsignedFloat('basic_salary')->default(0);
            $table->unsignedFloat('allowance_salary')->default(0);
            $table->unsignedFloat('gross_salary')->default(0);
            $table->string('image', 191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('employees');
    }
};
